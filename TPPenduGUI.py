# -*- coding: utf-8 -*-
"""
10/12/18
Mallol Fabien
Chabot Axel
TP3 Pendu Tkinter
"""
import random
import os
from Tkinter import *
from tkFont import Font
from PIL import Image, ImageTk


canvas = None
fenetre = None


def ouverture(file):  # Fonction ouverture aléatoire d'un mot dans le dictionnaire
    fichier = open(file, "r")  # Ouverture de fichier
    nbMots = sum(1 for line in fichier)  # Nb de mots dans le dico
    alea = random.randrange(1, nbMots + 1, 1)
    fichier.seek(0)
    for i in xrange(alea - 1):  # Lecture des lignes non utilisées
        fichier.readline()
    mot = fichier.readline().strip().lower()  # Mot retourné traité
    fichier.close()
    return mot


# Fonction pour générer les _ a la place des lettre connues et afficher la premiere lettre
def genererMotCache(mot):
    valRen = ''
    for i in mot:  # Création d'un mot de _ de la taille du mot connu
        valRen += '_'

    prem = mot[0]  # Premiere caractère : lettre
    # Si il y d'autres même lettres que la première
    for cpt, lettre in enumerate(mot):
        if lettre == prem:
            valRen = valRen[:cpt] + lettre + valRen[cpt + 1:]

    return valRen


def test(mot, lettre):  # Test si la lettre est dans le mot recherché
    if lettre in mot:
        return True
    else:
        return False


def remplacer(mot, motCache, lettre):  # Remplace le _ trouvé par sa bonne lettre
    listeIndice = []

    for cpt, car in enumerate(mot):
        if car == lettre:
            listeIndice.append(cpt)

    for cpt, car in enumerate(motCache):
        if cpt in listeIndice:
            motCache = motCache[:cpt] + lettre + motCache[cpt + 1:]

    return motCache


def error(liste, lettre, vies):  # Complete la liste de fausses lettre et enleve une vie
    if lettre not in liste:
        liste.append(lettre)
    vies -= 1
    return liste, vies


def restant(motCache):  # Calcul le nombre de _ restant
    valRen = 0
    for i in motCache:
        if i == '_':
            valRen += 1
    return valRen


def testVictoire(motCache):  # Dis si le joueur à gagné
    if restant(motCache) == 0:
        return True

    else:
        return False



def highScore(nbVict):
    fileName = "highScore"  # Nom de fichier
    if not os.path.isfile(fileName):  # Si le fichier n'est pas existant
        file = open(fileName, "w")  # Créé le fichier
        pseudo = raw_input("Entrer votre pseudo pour le high score : ")
        file.seek(0)
        file.write(pseudo + ":" + str(nbVict))

    else:
        file = open(fileName, "rw+")
        ligne = file.read().split(":")  # Sépare le nom du joueur et son score
        if ligne[1][-1] == '\n':  # Enleve le retour à la ligne s'il y en a un
            ligne[1] = ligne[1][:-1]
        if nbVict >= int(ligne[1]):  # Si nouveau high score
            pseudo = raw_input("Entrer votre pseudo pour le high score : ")
            file.seek(0)
            file.truncate()  # Supprime le contenu avant d'écrire
            file.write(pseudo + ":" + str(nbVict))
    file.close()


def createGUI(motCache, vies, lettresFausses):
    global fenetre
    global canvas
    global lettreVar
    fenetre = Tk()
    taille = len(mot)
    if taille >= 12:
        size = 26

    elif 12 > taille > 10:
        size = 30

    else:
        size = 38

    strange = Font(family="Strange Magic",size=size)
    strangeS = Font(family="Strange Magic",size=26)
    fenetre.title('Pendu')
    canvas = Canvas(fenetre, width=1500, height=800, background='white')
    titre = canvas.create_text(750, 30, text="PENDU", font=strangeS, fill="black")
    affMot = " ".join(motCache.upper())
    txt = canvas.create_text(400, 400, text=affMot, font=strange, fill='black')
    txt2 = canvas.create_text(100, 20, text="Nombre de vies : " + str(vies), font="Times 20", fill='black')
    lettreVar = StringVar()
    entree = Entry(canvas, textvariable=lettreVar)
    canvas.create_window(350, 500, window=entree, height=40, width=50)
    bouton = Button(canvas, text="Valider", command=button)
    canvas.create_window(420, 500, window=bouton, height=40, width=80)
    canvas.pack()
    fenetre.mainloop()


def updateGUI(motCache, mot,  vies, lettresFausses):
    global fenetre
    global canvas
    global strange
    global lettreVar
    canvas.delete('all')
    taille = len(mot)
    if taille >= 12:
        size = 26

    elif 12 > taille > 10:
        size = 30

    else:
        size = 38

    strange = Font(family="Strange Magic",size=size)
    strangeS = Font(family="Strange Magic",size=26)
    if not victoire and vies > 0:
        titre = canvas.create_text(750, 30, text="PENDU", font=strangeS, fill="black")
    elif victoire:
        titre = canvas.create_text(750, 30, text="GAGNE !", font=strangeS, fill="black")
    else:
        titre = canvas.create_text(750, 30, text="PERDU !", font=strangeS, fill="black")

    if vies > 0:
        affMot = " ".join(motCache.upper())
    else:
        affMot = " ".join(mot.upper())

    txt = canvas.create_text(400, 400, text=affMot, font=strange, fill='black')
    txt2 = canvas.create_text(100, 20, text="Nombre de vies : " + str(vies), font="Times 20", fill='black')
    if not victoire and vies > 0:
        lettreVar = StringVar()
        entree = Entry(canvas, textvariable=lettreVar)
        canvas.create_window(350, 500, window=entree, height=40, width=50)
        bouton = Button(canvas, text="Valider", command=button)
        canvas.create_window(420, 500, window=bouton, height=40, width=80)

    if not victoire and (0 < vies < 8):
        affFaux = ' '.join(lettresFausses).upper()
        txt3 = canvas.create_text(1200, 20, text="Lettres fausses : " + affFaux, font="Times 20", fill='black')
        im = Image.open("%s.gif" % (str(8-vies)))
        photo = ImageTk.PhotoImage(im)
        img = canvas.create_image(1100, 400, image=photo)

    elif victoire or vies == 0:
        bouton2 = Button(canvas, text="Restart ?", command=restart)
        canvas.create_window(1200, 350, window=bouton2, height=40, width=100)
        bouton3 = Button(canvas, text="Exit !", command=exit)
        canvas.create_window(1200, 400, window=bouton3, height=40, width=100)


    canvas.pack()
    fenetre.mainloop()

def restart():
    global fenetre
    global canvas
    global lettreVar
    global mot
    global vies
    global motCache
    global victoire
    global lettresFausses

    lettreVar = None
    mot = ouverture('dico')
    vies = 8
    motCache = genererMotCache(mot)
    victoire = False
    lettresFausses = []
    canvas.delete('all')
    updateGUI(motCache, mot, vies, lettresFausses)


def button():
    global lettreVar
    global mot
    global motCache
    global lettresFausses
    global victoire
    global vies

    lettre = lettreVar.get()
    presence = test(mot, lettre)

    if presence == True:
        motCache = remplacer(mot, motCache, lettre)
        victoire = testVictoire(motCache)
    else:
        lettresFausses, vies = error(lettresFausses, lettre, vies)
    print mot, vies, victoire, lettresFausses
    updateGUI(motCache, mot, vies, lettresFausses)




fenetre = None
canvas = None
lettreVar = None
mot = ouverture('dico')
vies = 8
motCache = genererMotCache(mot)
victoire = False
lettresFausses = []
createGUI(motCache, vies, lettresFausses)
